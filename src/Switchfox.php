<?php

declare(strict_types=1);

namespace SayHi\Switchfox;

use SayHi\Switchfox\Apis\Aggregation\V4_0\AggregationV4_0;
use SayHi\Switchfox\Apis\EeziAirtime\V1_0_0\EeziAirtimeV1_0_0;
use SayHi\Switchfox\Apis\Electricity\V1_0_0\ElectricityV1_0_0;
use SayHi\Switchfox\Apis\Voucher\V3_0_0\VoucherV3_0_0;
use SayHi\Switchfox\Exceptions\ApiVersionMismatchException;
use SayHi\Switchfox\Exceptions\InvalidConfigException;

/**
 * Class Switchfox
 */
class Switchfox
{
    public string $host;
    protected string $consumerToken;
    protected string $accountNumber;

    protected bool $cacheEnabled;
    protected int $cacheGiftVoucherLifetime;
    protected int $cacheEeziAirtimeLifetime;
    protected int $cacheProductsLifetime;

    protected array $networks;

    /**
     * @throws InvalidConfigException
     */
    public function __construct(?string $consumerToken = null,
        ?string $accountNumber = null,
        ?string $host = null,
        ?bool $cacheEnabled = null,
        ?int $cacheGiftVoucherLifetime = null,
        ?int $cacheEeziAirtimeLifetime = null,
        ?int $cacheProductsLifetime = null,
        ?array $networks = null
    ) {
        $this->consumerToken = $consumerToken ?? config('switchfox.auth.consumer_token');
        $this->accountNumber = $accountNumber ?? config('switchfox.auth.account_number');
        $this->host = $host ?? config('switchfox.host');

        $this->cacheEnabled = $cacheEnabled ?? config('switchfox.cache.enabled');
        $this->cacheGiftVoucherLifetime = $cacheGiftVoucherLifetime ?? config('switchfox.cache.lifetime.gift_vouchers');
        $this->cacheEeziAirtimeLifetime = $cacheEeziAirtimeLifetime ?? config('switchfox.cache.lifetime.eezi_airtime');
        $this->cacheProductsLifetime = $cacheProductsLifetime ?? config('switchfox.cache.lifetime.products');
        $this->networks = $networks ?? config('switchfox.networks');

        $this->validateConfig();
    }

    /**
     * @throws ApiVersionMismatchException
     */
    public function eeziAirtime(string $version = '1.0.0')
    {
        switch ($version) {
            case '1.0.0':
                return resolve(EeziAirtimeV1_0_0::class, ['properties' => get_object_vars($this)]);
                break;
            default:
                throw new ApiVersionMismatchException("The requested version wasn't found");
        }
    }

    /**
     * @throws ApiVersionMismatchException
     */
    public function electricity(string $version = '1.0.0')
    {
        switch ($version) {
            case '1.0.0':
                return resolve(ElectricityV1_0_0::class, ['properties' => get_object_vars($this)]);
                break;
            default:
                throw new ApiVersionMismatchException("The requested version wasn't found");
        }
    }

    /**
     * @throws ApiVersionMismatchException
     */
    public function voucher(string $version = '3.0.0')
    {
        switch ($version) {
            case '3.0.0':
                return resolve(VoucherV3_0_0::class, ['properties' => get_object_vars($this)]);
                break;
            default:
                throw new ApiVersionMismatchException("The requested version wasn't found");
        }
    }

    /**
     * @throws ApiVersionMismatchException
     */
    public function aggregation(string $version = '4.0')
    {
        switch ($version) {
            case '4.0':
                return resolve(AggregationV4_0::class, ['properties' => get_object_vars($this)]);
                break;
            default:
                throw new ApiVersionMismatchException("The requested version wasn't found");
        }
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfig(): void
    {
        $this->validateConfigAuth();
        $this->validateConfigHost();
        $this->validateConfigCache();
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfigAuth()
    {
        if (! $this->consumerToken) {
            throw new InvalidConfigException('Unable to find or create Switchfox Consumer Token in your config!');
        }
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfigHost()
    {
        if (! $this->host) {
            throw new InvalidConfigException('Unable to find Switchfox Host in your config!');
        }
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfigCache()
    {
        if (! is_numeric($this->cacheGiftVoucherLifetime)) {
            throw new InvalidConfigException('Cache Gift Voucher Lifetime must be set to the cache duration in seconds');
        }
    }
}
