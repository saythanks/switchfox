<?php

declare(strict_types=1);

return

    /*
    |--------------------------------------------------------------------------
    | Switchfox API Settings
    |--------------------------------------------------------------------------
    |
    |
    */
    [

        /*
        |--------------------------------------------------------------------------
        | Authentication
        |--------------------------------------------------------------------------
        | Switchfox API supports username/password as well as consumer tokens.
        | Currently this api supports only the token authentication.
        | consumer_token is generated as a base64 of the key and the secret with a
        | colon between them.
        | Either a token or both the key and secret are required.
        | Note that the key dictates if you're on sandbox or production, so make
        | sure that you're using the right key for the right environment
        |
        |
        */
        'auth' => [
            'consumer_token' => env('SWITCHFOX_CONSUMER_TOKEN', base64_encode(env('SWITCHFOX_CONSUMER_KEY') . ':' . env('SWITCHFOX_CONSUMER_SECRET'))),
            'consumer_key' => env('SWITCHFOX_CONSUMER_KEY', ''), // Consumer Key, required if CONSUMER_TOKEN isn't set)
            'consumer_secret' => env('SWITCHFOX_CONSUMER_SECRET', ''), // Consumer Secret, required if CONSUMER_TOKEN isn't set)
            'account_number' => env('SWITCHFOX_ACCOUNT_NUMBER', ''),
        ],

        'host' => env('SWITCHFOX_HOST', 'https://api.flashswitch.flash-group.com'),

        /*
        |--------------------------------------------------------------------------
        | Caching
        |--------------------------------------------------------------------------
        | If enabled, the following objects will be cached:
        | * access token - Access Tokens are cached for 1 minute less than they last
        | * cellular bundles - Bundles are cached with a default duration of 1 day
        | * gift vouchers - Vouchers are cached with a default duration of 1 day
        |
        | Note that caching uses tags, make sure your cache mechanism supports this
        | https://laravel.com/docs/7.x/cache#cache-tags
        |
        */

        'cache' => [
            'enabled' => env('SWITCHFOX_CACHING_ENABLED', false),
            'lifetime' => [
                'default' => env('SWITCHFOX_CACHING_LIFETIME_DEFAULT', 86400),
                'gift_vouchers' => env('SWITCHFOX_CACHING_LIFETIME_GIFTS', env('SWITCHFOX_CACHING_LIFETIME_DEFAULT', 86400)),
                'eezi_airtime' => env('SWITCHFOX_CACHING_LIFETIME_EEZI_AIRTIME', env('SWITCHFOX_CACHING_LIFETIME_DEFAULT', 86400)),
                'products' => env('SWITCHFOX_CACHING_LIFETIME_PRODUCTS', env('SWITCHFOX_CACHING_LIFETIME_DEFAULT', 86400)),
            ],
        ],

        /*
        |--------------------------------------------------------------------------
        | Networks
        |--------------------------------------------------------------------------
        |
        | Current supported networks.
        | The Key is the code looked at and the value is A Display Name
        | Note that 8ta bundles are string replaced to Telkom
        |
        */
        'networks' => [
            'options' => [
                'CELL C' => 'Cell C',
                'MTN' => 'MTN',
                'TELKOM' => 'Telkom',
                'VIRGIN MOBILE' => 'Virgin Mobile',
                'VODACOM' => 'Vodacom',
                'LYCAMOBILE' => 'Lycamobile',
            ],
        ],

        /*
        |--------------------------------------------------------------------------
        | Transaction Logging
        |--------------------------------------------------------------------------
        |
        | Currently only the database driver is available here.
        | If this is enabled and the driver is `database`, please re-run your migrations!
        |
        |
        */
        'transaction_logging' => [
            'enabled' => env('SWITCHFOX_TRANSACTION_ENABLED', false),
            'driver' => env('SWITCHFOX_TRANSACTION_DRIVER', 'database'),
            'prefix' => env('SWITCHFOX_TRANSACTION_TABLE_PREFIX', 'switchfox_audit_'),
        ],
    ];
