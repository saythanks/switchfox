<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Console\Commands;

use Illuminate\Console\Command;
use SayHi\Switchfox\Switchfox;

class FlushCacheCommand extends Command
{
    protected $signature = 'switchfox:cache-flush';

    protected $description = 'Flush the switchfox cache';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $service = resolve(Switchfox::class);

        $service->electricity('1.0.0')->flushCache();
        $service->voucher('3.0.0')->flushCache();

        return self::SUCCESS;
    }
}
