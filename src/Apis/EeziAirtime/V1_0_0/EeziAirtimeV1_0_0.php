<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Apis\EeziAirtime\V1_0_0;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SayHi\Switchfox\Apis\FlashSwitchApi;
use SayHi\Switchfox\Dto\ResponseTransactionDto;

class EeziAirtimeV1_0_0 extends FlashSwitchApi
{
    public string $api_name = 'EeziAirtime';
    public string $api_url = 'eezi/airtime';
    public string $api_version = '1.0.0';
    public array $tags = ['switchfox', 'eezi-airtime'];

    public function getProducts()
    {
        $url = "/{$this->api_url}/{$this->api_version}/Products/{$this->accountNumber}";
        $data = $this->getFromCache('eezi-airtime_1.0.0_products');
        if ($data !== false) {
            return $data;
        }
        Log::debug('Switchfox-API: External call to get EeziAirtime Products');
        $response = $this->switchfoxApi()->get($this->host . $url);
        $this->checkForErrors($response);

        $products = collect($response->json()['data']);
        $this->storeInCache('eezi-airtime_1.0.0_products', $products, $this->cacheEeziAirtimeLifetime);

        return $products;
    }

    public function purchase(int $amountInCents, string $productCode, $reference = null): array
    {
        $referenceId = $reference ?? Str::uuid()->toString();
        $url = "/{$this->api_url}/{$this->api_version}/Purchase";
        $data = [
            'reference' => $referenceId,
            'amount' => $amountInCents,
            'productCode' => $productCode,
            'accountNumber' => $this->accountNumber,
        ];

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-purchase',
            endpoint: $url,
            amountInCents: $amountInCents,
            referenceId: $referenceId,
            requestData: $data);

        $response = $this->switchfoxApi()->post($this->host . $url, $data);
        $this->updateTransaction($transaction, $this->cleanAirtimeResponse($response));

        $this->checkForErrors($response);

        return $response->json();
    }

    protected function cleanAirtimeResponse(Response $response): ResponseTransactionDto
    {
        return new ResponseTransactionDto(
            external_id: Arr::get($response->json(), 'data.traceIdentifier'),
            response_code: Arr::get($response->json(), 'code'),
            response_message: Arr::get($response->json(), 'message'),
            response_datetime: Arr::get($response->json(), 'data.transaction.created'),
            response_data: [
                'transaction_reference' => Arr::get($response->json(), 'data.transaction.reference'),
                'voucher' => Arr::get($response->json(), 'data.voucher'),
            ]
        );
    }
}
