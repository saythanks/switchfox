<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Apis;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SayHi\Switchfox\Exceptions\AuthorizationException;
use SayHi\Switchfox\Exceptions\GenericException;
use SayHi\Switchfox\Transactions\InteractsWithTransactions;

class FlashSwitchApi
{
    use InteractsWithTransactions;

    public string $api_name = '';
    public string $api_url = '';
    public string $api_version = '';
    public array $tags = ['switchfox'];

    protected string $host;
    protected string $accessToken;
    protected string $consumerToken;
    protected string $accountNumber;

    protected bool $cacheEnabled;
    protected int $cacheGiftVoucherLifetime;
    protected int $cacheProductsLifetime;
    protected int $cacheEeziAirtimeLifetime;

    public function __construct(array $properties)
    {
        foreach ($properties as $key => $property) {
            $this->$key = $property;
        }
    }

    public function getApiName(): string
    {
        return $this->api_name . '-' . $this->api_version;
    }

    public function getAccessToken()
    {
        $data = $this->getFromCache('access_token');
        if ($data !== false) {
            $this->accessToken = $data;

            return $this->accessToken;
        }

        Log::debug('Switchfox-API: External call to get Access Token');
        $response = Http::withHeaders([
            'Authorization' => "Basic {$this->consumerToken}",
        ])->asForm()->post("{$this->host}/token", [
            'grant_type' => 'client_credentials',
        ]);
        if (! isset($response->json()['access_token'])) {
            throw new AuthorizationException($response);
        }
        $this->checkForErrors($response);

        $this->accessToken = $response->json()['access_token'];

        // This time is set to 1 minute less than the token lives for from switchfox's side
        $this->storeInCache('access_token', $this->accessToken, Arr::get($response->json(), 'expires_in') - 60);

        return $this->accessToken;
    }

    /**
     * @param  array|string[]  $tags
     */
    public function flushCache(array $tags = ['switchfox']): bool
    {
        if ($this->cacheEnabled) {
            Log::debug('Switchfox-API: Cache Flushed', ['tags' => $tags]);
            Cache::tags($tags)->flush();

            return true;
        }

        return false;
    }

    /**
     * This will return an increasing int counter if cache is enabled otherwise a UUID
     *
     * @return string
     */
    public function generateReferenceId()
    {
        if ($this->cacheEnabled) {
            return Cache::increment('switchfox-reference-id-counter');
        }

        return Str::uuid()->toString();
    }

    /**
     * @return PendingRequest
     *
     * @throws AuthorizationException|GenericException
     */
    public function switchfoxApi()
    {
        return Http::withHeaders([
            'Authorization' => "Bearer {$this->getAccessToken()}",
        ])
            ->asJson()
            ->acceptJson();
    }

    /**
     * @param  array|string[]  $tags
     * @return mixed
     */
    protected function getFromCache(string $key)
    {
        if ($this->cacheEnabled) {
            if (Cache::tags($this->tags)->has($key)) {
                Log::debug('Switchfox-API: Retrieving collection from cache', ['tags' => $this->tags, 'key' => $key]);

                return Cache::tags($this->tags)->get($key);
            }
        }

        return false;
    }

    /**
     * @param  array|string[]  $tags
     */
    protected function storeInCache(string $key, $data, $seconds): bool
    {
        if ($this->cacheEnabled) {
            Log::debug('Switchfox-API: Saving to cache', ['tags' => $this->tags, 'key' => $key]);

            return Cache::tags($this->tags)->put($key, $data, $seconds);
        }

        return false;
    }

    /**
     * @throws AuthorizationException|GenericException
     */
    protected function checkForErrors(Response $response)
    {
        if ($response->status() == 401) {
            throw new AuthorizationException($response);
        }
        if ($response->json() && isset($response->json()['fault'])) {
            if (Arr::get($response->json(), 'fault.message') && Arr::get($response->json(), 'fault.description')) {
                Log::error('Switchfox-Api: Fault detected!', ['message' => Arr::get($response->json(), 'fault.message'), 'description' => Arr::get($response->json(), 'fault.description'), 'code' => Arr::get($response->json(), 'fault.code'), 'response_json' => $response->json()]);
                throw new GenericException('Switchfox-Api: ' . Arr::get($response->json(), 'fault.code') . ': ' . Arr::get($response->json(), 'fault.message') . ' : ' . Arr::get($response->json(), 'fault.description'));
            }
            Log::error('Switchfox-Api: fault detected but no message/description found', ['response_json' => $response->json()]);
            throw new GenericException('Switchfox-Api: Fault detected');
        }

        return true;
    }

    /**
     * This will generate a sequence number based on the microtime
     * The API requires these numbers to be unique for a 30 minute period
     *
     * @return int
     */
    protected function generateSequenceNumber()
    {
        return microtime(true);
    }
}
