<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Apis\Voucher\V3_0_0;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SayHi\Switchfox\Apis\FlashSwitchApi;
use SayHi\Switchfox\Dto\ResponseTransactionDto;
use SayHi\Switchfox\Exceptions\WalletBalanceException;

class VoucherV3_0_0 extends FlashSwitchApi
{
    public string $api_name = 'Voucher';
    public string $api_url = 'voucher';
    public string $api_version = '3.0.0';
    public array $tags = ['switchfox', 'voucher'];

    public function getProducts(bool $fresh = false)
    {
        $url = "/{$this->api_url}/{$this->api_version}/Products/{$this->accountNumber}";
        $cacheKey = '3.0.0_vouchers_product';

        if (! $fresh) {
            $data = $this->getFromCache($cacheKey);
            if ($data !== false) {
                return $data;
            }
        }

        Log::debug('Switchfox-API: VoucherV3_0_0 - Products');
        $response = $this->switchfoxApi()->get($this->host . $url);
        $this->checkForErrors($response);

        $giftVouchers = collect($response->json()['data']);
        $this->storeInCache($cacheKey, $giftVouchers, $this->cacheGiftVoucherLifetime);

        return $giftVouchers;
    }

    public function getProductInfo(bool $fresh = false)
    {
        $url = "/{$this->api_url}/{$this->api_version}/GetProductInfo/{$this->accountNumber}";
        $cacheKey = '3.0.0_vouchers_product_info';

        if (! $fresh) {
            $data = $this->getFromCache($cacheKey);
            if ($data !== false) {
                return $data;
            }
        }
        Log::debug('Switchfox-API: VoucherV3_0_0 - GetProductInfo');
        $response = $this->switchfoxApi()->get($this->host . $url);
        $this->checkForErrors($response);

        $giftVouchers = collect($response->json()['data']);
        $this->storeInCache($cacheKey, $giftVouchers, $this->cacheGiftVoucherLifetime);

        return $giftVouchers;
    }

    public function purchase(int $amountInCents, string $productCode, $referenceId = null): array
    {
        $referenceId = $referenceId ?? Str::uuid()->toString();
        $url = "/{$this->api_url}/{$this->api_version}/Purchase";
        $data = [
            'reference' => $referenceId,
            'amount' => $amountInCents,
            'productCode' => $productCode,
            'subAccountNumber' => $this->accountNumber,
            'metadata' => [
                'userId' => 'defaultSayThanksUser',
            ],
        ];

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-purchase',
            endpoint: $url,
            amountInCents: $amountInCents,
            referenceId: $referenceId,
            requestData: $data);

        $response = $this->switchfoxApi()->retry(3)->post($this->host . $url, $data);

        $this->checkForErrors($response);
        if ($response->json('data.transaction.id') === null) {
            Log::error('Switchfox-Api: Unhandled Error Detected!', ['code' => $response->json('code'), 'response' => $response->json(), 'message' => $response->json('message')]);
        }

        $this->updateTransaction($transaction, $this->cleanPurchaseResponse($response));

        return $response->json();
    }

    public function cancel(string $serial, string $productCode, ?string $referenceId = null): array
    {
        $referenceId = $referenceId ?? Str::uuid()->toString();
        $url = "/{$this->api_url}/{$this->api_version}/Cancel";
        $data = [
            'reference' => $referenceId ?? Str::uuid()->toString(),
            'serial' => $serial,
            'productCode' => $productCode,
            'subAccountNumber' => $this->accountNumber,
            'metadata' => [
                'userId' => 'defaultSayThanksUser',
            ],
        ];

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-cancel',
            endpoint: $url,
            amountInCents: 0,
            referenceId: $referenceId,
            requestData: $data);

        $response = $this->switchfoxApi()->retry(3)->post($this->host . $url, $data);
        $this->updateTransaction($transaction, $this->cleanApiResponse($response));

        $this->checkForErrors($response);

        return $response->json();
    }

    protected function cleanPurchaseResponse(Response $response): ResponseTransactionDto
    {
        return new ResponseTransactionDto(
            external_id: $response->json('data.transaction.id'),
            response_code: $response->json('code'),
            response_message: $response->json('message'),
            response_datetime: $response->json('data.transaction.created'),
            response_data: [
                'duplicate' => $response->json('data.duplicate'),
                'transaction' => $response->json('data.transaction'),
                'product' => $response->json('data.product'),
                'voucher_purchase_response' => $response->json('data.voucherPurchaseResponse'),
                'voucher' => $response->json('data.voucher'),
            ]
        );
    }

    protected function checkForErrors(Response $response): bool
    {
        if ($response->json() && $response->json('code') === 2003) { //
            Log::error('Switchfox-Api: Fault detected!', ['code' => $response->json('code'), 'data' => $response->json('data'), 'message' => $response->json('message')]);
            throw new WalletBalanceException('Switchfox-Api: ' . $response->json('code') . ': ' . $response->json('message') . ' - ' . $response->json('message'));
        }

        return parent::checkForErrors($response);
    }
}
