<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Apis\Aggregation\V4_0;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SayHi\Switchfox\Apis\FlashSwitchApi;
use SayHi\Switchfox\Dto\ResponseTransactionDto;
use SayHi\Switchfox\Exceptions\InvalidAccountException;
use SayHi\Switchfox\Exceptions\InvalidSwitchFoxResponseException;
use SayHi\Switchfox\Exceptions\WalletBalanceException;
use Throwable;

class AggregationV4_0 extends FlashSwitchApi
{
    public string $api_name = 'Aggregation';
    public string $api_url = 'aggregation';
    public string $api_version = '4.0';
    public array $tags = ['switchfox', 'aggregation'];

    public function listProducts(bool $fresh = false)
    {
        $url = "/{$this->api_url}/{$this->api_version}/accounts/{$this->accountNumber}/products";
        $cacheKey = 'aggregation_4_0_product';

        if (! $fresh) {
            $data = $this->getFromCache($cacheKey);
            if ($data !== false) {
                return $data;
            }
        }

        Log::debug('Switchfox-API: AggregationV4_0 - List Products');
        $response = $this->switchfoxApi()->get($this->host . $url);
        $this->checkForErrors($response);

        $products = collect($response->json());
        $this->storeInCache($cacheKey, $products, $this->cacheGiftVoucherLifetime);

        return $products;
    }

    public function lookupProduct(int $productCode)
    {
        $url = "/{$this->api_url}/{$this->api_version}/accounts/{$this->accountNumber}/products/{$productCode}";

        Log::debug('Switchfox-API: AggregationV4_0 - Lookup Product');
        $response = $this->switchfoxApi()->get($this->host . $url);
        $this->checkForErrors($response);

        return $response->json();
    }

    public function purchase1Voucher(int $amountInCents, ?string $referenceId = null, ?string $accountNumber = null, array $metadata = []): array
    {
        $referenceId = $referenceId ?? Str::uuid()->toString();
        $accountNumber = $accountNumber ?? $this->accountNumber;
        $url = "/{$this->api_url}/{$this->api_version}/1voucher/purchase";
        $data = [
            'reference' => $referenceId,
            'accountNumber' => $accountNumber,
            'amount' => $amountInCents,
        ];
        if (count($metadata) > 0) {
            $data['metadata'] = $metadata;
        }

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-1Voucher-Purchase',
            endpoint: $url,
            amountInCents: $amountInCents,
            referenceId: $referenceId,
            requestData: $data);

        $response = $this->switchfoxApi()->retry(3)->post($this->host . $url, $data);

        $this->checkForErrors($response);
        if ($response->json('transactionId') === null) {
            Log::error('Switchfox-Api: Unhandled Error Detected!', ['code' => $response->json('code'), 'response' => $response->json(), 'message' => $response->json()]);
        }
        Log::debug('Switchfox-API: AggregationV4_0 - 1Voucher Purchase', ['response' => $response->json()]);

        $this->updateTransaction($transaction, $this->clean1VoucherPurchaseResponse($response));

        return $response->json();
    }

    public function disburse1Voucher(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function redeem1Voucher(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function refund1Voucher(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function purchaseGiftVoucher(int $amountInCents, int $productCode, string $storeId, string $terminalId, ?string $referenceId = null, ?string $accountNumber = null, array $metadata = []): array
    {
        $referenceId = $referenceId ?? Str::uuid()->toString();
        $accountNumber = $accountNumber ?? $this->accountNumber;
        $url = "/{$this->api_url}/{$this->api_version}/gift-voucher/purchase";
        $data = [
            'reference' => $referenceId,
            'accountNumber' => $accountNumber,
            'amount' => $amountInCents,
            'productCode' => $productCode,
            'storeId' => $storeId,
            'terminalId' => $terminalId,
        ];
        if (count($metadata) > 0) {
            $data['metadata'] = $metadata;
        }

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-GiftVoucher-Purchase',
            endpoint: $url,
            amountInCents: $amountInCents,
            referenceId: $referenceId,
            requestData: $data);

        $response = $this->switchfoxApi()->post($this->host . $url, $data);

        $this->checkForErrors($response);
        if ($response->json('transactionId') === null) {
            Log::error('Switchfox-Api: Unhandled Error Detected!', ['code' => $response->json('code'), 'response' => $response->json(), 'message' => $response->json()]);
        }
        Log::debug('Switchfox-API: AggregationV4_0 - GiftVoucher Purchase', ['data' => $data, 'response' => $response->json()]);

        $this->updateTransaction($transaction, $this->cleanGiftVoucherPurchaseResponse($response));

        return $response->json();
    }

    public function purchaseCashOutPin(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function cancelCashOutPin(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function lookupCashOutPin(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function purchaseCellularPinless(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function purchaseEeziVoucher(int $amountInCents, int $productCode, ?string $storeId = null, ?string $terminalId = null, $referenceId = null, ?string $accountNumber = null, array $metadata = []): array
    {
        $referenceId = $referenceId ?? Str::uuid()->toString();
        $accountNumber = $accountNumber ?? $this->accountNumber;
        $url = "/{$this->api_url}/{$this->api_version}/eezi-voucher/purchase";
        $data = [
            'reference' => $referenceId,
            'accountNumber' => $accountNumber,
            'amount' => $amountInCents,
            'productCode' => $productCode,
        ];
        if (count($metadata) > 0) {
            $data['metadata'] = $metadata;
        }
        if ($storeId !== null) {
            $data['storeId'] = $storeId;
        }
        if ($terminalId !== null) {
            $data['terminalId'] = $terminalId;
        }

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-EeziVoucher-Purchase',
            endpoint: $url,
            amountInCents: $amountInCents,
            referenceId: $referenceId,
            requestData: $data);

        $response = $this->switchfoxApi()->post($this->host . $url, $data);

        $this->checkForErrors($response);
        if ($response->json('transactionId') === null) {
            Log::error('Switchfox-Api: Unhandled Error Detected!', ['code' => $response->json('code'), 'response' => $response->json(), 'message' => $response->json()]);
        }
        Log::debug('Switchfox-API: AggregationV4_0 - EeziVoucher Purchase', ['data' => $data, 'response' => $response->json()]);

        $this->updateTransaction($transaction, $this->cleanEeziVoucherPurchaseResponse($response));

        return $response->json();
    }

    public function lookupPrepaidUtility(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function purchasePrepaidUtility(int $amountInCents, string $productCode, $referenceId = null): array
    {
        return [];
    }

    public function cancel(string $serial, string $productCode, ?string $referenceId = null): array
    {
        $referenceId = $referenceId ?? Str::uuid()->toString();
        $url = "/{$this->api_url}/{$this->api_version}/Cancel";
        $data = [
            'reference' => $referenceId ?? Str::uuid()->toString(),
            'serial' => $serial,
            'productCode' => $productCode,
            'subAccountNumber' => $this->accountNumber,
            'metadata' => [
                'userId' => 'defaultSayThanksUser',
            ],
        ];

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-cancel',
            endpoint: $url,
            amountInCents: 0,
            referenceId: $referenceId,
            requestData: $data);

        $response = $this->switchfoxApi()->retry(3)->post($this->host . $url, $data);
        if ($response->json('transactionId') === null) {
            Log::error('Switchfox-Api: Unhandled Error Detected!', ['code' => $response->json('code'), 'response' => $response->json(), 'message' => $response->json()]);
        }
        $this->updateTransaction($transaction, $this->cleanApiResponse($response));

        $this->checkForErrors($response);

        return $response->json();
    }

    protected function clean1VoucherPurchaseResponse(Response $response): ResponseTransactionDto
    {
        return new ResponseTransactionDto(
            external_id: (string) $response->json('transactionId'),
            response_code: (string) $response->json('responseCode'),
            response_message: $response->json('responseMessage'),
            response_datetime: $response->json('transactionDate'),
            response_data: [
                'amount' => $response->json('amount'),
                'voucher' => $response->json('voucher'),
            ]
        );
    }

    /**
     * @throws InvalidSwitchFoxResponseException
     */
    protected function cleanGiftVoucherPurchaseResponse(Response $response): ResponseTransactionDto
    {
        try {
            return new ResponseTransactionDto(
                external_id: (string) $response->json('transactionId'),
                response_code: (string) $response->json('responseCode'),
                response_message: $response->json('responseMessage'),
                response_datetime: $response->json('transactionDate'),
                response_data: [
                    'amount' => $response->json('amount'),
                    'voucher' => $response->json('voucher'),
                    'metadata' => [
                        'storeId' => $response->json('storeId'),
                        'terminalId' => $response->json('terminalId'),
                        'reference' => $response->json('reference'),
                    ],
                ]
            );
        } catch (Throwable $throwable) {
            Log::error('Switchfox-Api V4 Aggregation: Gift Voucher Error!', ['exception_message' => $throwable->getMessage(), 'response' => $response->json()]);
            throw new InvalidSwitchFoxResponseException(message: 'Invalid Gift Voucher Response from V4 Aggregation API', previous: $throwable);
        }

    }

    /**
     * @throws InvalidSwitchFoxResponseException
     */
    protected function cleanEeziVoucherPurchaseResponse(Response $response): ResponseTransactionDto
    {
        try {
            return new ResponseTransactionDto(
                external_id: (string) $response->json('transactionId'),
                response_code: (string) $response->json('responseCode'),
                response_message: $response->json('responseMessage'),
                response_datetime: $response->json('transactionDate'),
                response_data: [
                    'amount' => $response->json('amount'),
                    'voucher' => $response->json('voucher'),
                ]
            );
        } catch (Throwable $throwable) {
            Log::error('Switchfox-Api V4 Aggregation: Eezi Voucher Error!', ['exception_message' => $throwable->getMessage(), 'response' => $response->json()]);
            throw new InvalidSwitchFoxResponseException(message: 'Invalid Eezi Voucher Response from V4 Aggregation API', previous: $throwable);
        }
    }

    protected function checkForErrors(Response $response): bool
    {
        if ($response->json() && $response->json('code') === 2003) { //
            Log::error('Switchfox-Api: Fault detected!', ['code' => $response->json('code'), 'data' => $response->json('data'), 'message' => $response->json('message')]);
            throw new WalletBalanceException('Switchfox-Api: ' . $response->json('code') . ': ' . $response->json('message') . ' - ' . $response->json('message'));
        }
        if ($response->json() && $response->json('responseCode') === 2270) { //
            Log::error('Switchfox-Api: Invalid Account Number!', ['code' => $response->json('responseCode'), 'data' => $response->json(), 'message' => $response->json('responseMessage')]);
            throw new InvalidAccountException('Switchfox-Api: Invalid Account Number!' . $response->json('responseCode') . ': ' . $response->json('responseMessage') . ' - ' . $response->json('message'));
        }

        return parent::checkForErrors($response);
    }
}
