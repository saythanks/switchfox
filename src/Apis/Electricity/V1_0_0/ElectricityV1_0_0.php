<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Apis\Electricity\V1_0_0;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SayHi\Switchfox\Apis\FlashSwitchApi;
use SayHi\Switchfox\Dto\ResponseTransactionDto;

class ElectricityV1_0_0 extends FlashSwitchApi
{
    public string $api_name = 'Electricity';
    public string $api_url = 'electricity';
    public string $api_version = '1.0.0';

    public function lookup(string $meterNumber, ?string $requestId = null): array
    {
        $url = "{$this->api_url}/{$this->api_version}/{$this->accountNumber}/lookup";

        $data = [
            'requestId' => $requestId ?? Str::uuid()->toString(),
            'meterNumber' => $meterNumber,
            'acquirer' => [
                'account' => [
                    'accountNumber' => $this->accountNumber,
                ],
            ],
        ];
        Log::withContext(['meterNumber' => $meterNumber, 'requestId' => $requestId]);
        Log::debug('Switchfox-API: External call to Lookup Meter Number');
        $response = $this->switchfoxApi()->post($this->host . $url, $data);
        $this->checkForErrors($response);

        return $response->json();
    }

    public function purchase(string $meterNumber, int $amountInCents, $requestId = null): array
    {
        $url = "/{$this->api_url}/{$this->api_version}/purchase";
        $requestId = $requestId ?? Str::uuid()->toString();
        $data = [
            'requestId' => $requestId,
            'meterNumber' => $meterNumber,
            'amount' => $amountInCents,
            'acquirer' => [
                'account' => [
                    'accountNumber' => $this->accountNumber,
                ],
            ],
        ];

        $transaction = $this->createTransaction(
            type: $this->getApiName() . '-purchase',
            endpoint: $url,
            amountInCents: $amountInCents,
            referenceId: $requestId,
            requestData: $data);

        $response = $this->switchfoxApi()->post($this->host . $url, $data);
        $this->updateTransaction($transaction, $this->cleanApiResponse($response));

        $this->checkForErrors($response);

        return $response->json();
    }

    protected function cleanApiResponse(Response $response): ResponseTransactionDto
    {
        return new ResponseTransactionDto(
            external_id: Arr::get($response->json(), 'transactionID'),
            response_code: Arr::get($response->json(), 'responseCode'),
            response_message: Arr::get($response->json(), 'responseMessage'),
            response_datetime: Arr::get($response->json(), 'transactionDate'),
            response_data: [
                'billing_info' => Arr::get($response->json(), 'billingInfo'),
                'meterInfo' => Arr::get($response->json(), 'meterInfo'),
                'tokens' => Arr::get($response->json(), 'tokens'),
            ]
        );
    }
}
