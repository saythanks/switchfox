<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Exceptions;

use Exception;
use Illuminate\Http\Client\Response;
use Throwable;

class AuthorizationException extends Exception
{
    protected Response $response;

    public function __construct(Response $response, $code = 0, ?Throwable $previous = null)
    {
        $this->response = $response;
        $message = 'Switchfox-Api: ';
        if ($response->json() && $response->json()['error'] && $response->json()['error_description']) {
            $message .= $response->json()['error'] . ': ' . $response->json()['error_description'];
        } elseif ($response->status() == 401) {
            $message .= 'Token Rejected by SwitchFox';
        } else {
            $message .= 'Unspecified error';
        }

        if ($response->status() && $code == 0) {
            $code = $response->status();
        }
        parent::__construct($message, $code, $previous);
    }
}
