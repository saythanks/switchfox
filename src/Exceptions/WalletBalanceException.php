<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Exceptions;

use Exception;

class WalletBalanceException extends Exception {}
