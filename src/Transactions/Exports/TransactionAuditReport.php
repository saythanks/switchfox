<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Transactions\Exports;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use SayHi\Switchfox\Transactions\SwitchFoxTransaction;

class TransactionAuditReport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    protected $dateFrom;
    protected $dateTo;

    public function __construct(Carbon $dateFrom, Carbon $dateTo)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function query()
    {
        return SwitchFoxTransaction::query()->whereBetween('created_at', [$this->dateFrom, $this->dateTo]);
    }

    public function headings(): array
    {
        return [
            'Client ID',
            'Product ID',
            'Product Description',
            '[Client] unique transaction reference',
            '[Flashswitch] unique transaction reference',
            'Transaction amount',
            'Transaction date',
            'Transaction time',
            'Successful or Unsuccessful (Response Code)',
            'Unsuccessful Response (Response Message)',
        ];
    }

    public function map($transaction): array
    {
        return [
            $transaction->user_id,
            null,
            null,
            $transaction->getKey(),
            $transaction->transaction_id,
            $transaction->amount,
            $transaction->created_at->toDateString(),
            $transaction->created_at->toTimeString(),
            $transaction->response_code,
            $transaction->response_message,
        ];
    }
}
