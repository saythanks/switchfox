<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Transactions;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use SayHi\Switchfox\Dto\ResponseTransactionDto;
use SayHi\Switchfox\SwitchfoxServiceProvider;

trait InteractsWithTransactions
{
    protected function createTransaction(string $type, string $endpoint = '', int $amountInCents = 0, string $referenceId = '', array $requestData = []): ?SwitchFoxTransaction
    {
        if (! config('switchfox.transaction_logging.enabled') || config('switchfox.transaction_logging.driver') !== SwitchfoxServiceProvider::DRIVER_DATABASE) {
            return null;
        }

        return SwitchFoxTransaction::create([
            'type' => $type,
            'endpoint' => $endpoint,
            'amount' => $amountInCents,
            'internal_id' => $referenceId,
            'request_data' => $requestData,
        ]);
    }

    protected function updateTransaction(?SwitchFoxTransaction $transaction, ResponseTransactionDto $responseTransactionDto): ?SwitchFoxTransaction
    {
        if (! config('switchfox.transaction_logging.enabled')
            || config('switchfox.transaction_logging.driver') !== SwitchfoxServiceProvider::DRIVER_DATABASE
            || is_null($transaction)) {
            return null;
        }
        $transaction->external_id = $responseTransactionDto->external_id;
        $transaction->response_code = $responseTransactionDto->response_code;
        $transaction->response_message = $responseTransactionDto->response_message;
        $transaction->response_datetime = $responseTransactionDto->response_datetime;
        $transaction->response_data = $responseTransactionDto->response_data;
        $transaction->save();

        return $transaction;
    }

    protected function cleanApiResponse(Response $response): ResponseTransactionDto
    {
        return new ResponseTransactionDto(
            external_id: Arr::get($response->json(), 'transactionID'),
            response_code: Arr::get($response->json(), 'responseCode'),
            response_message: Arr::get($response->json(), 'responseMessage'),
            response_datetime: Arr::get($response->json(), 'responseTime'),
            response_data: [
                'response_time' => Arr::get($response->json(), 'responseTime'),
            ]
        );
    }
}
