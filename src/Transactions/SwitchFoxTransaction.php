<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Transactions;

use Illuminate\Database\Eloquent\Model;

class SwitchFoxTransaction extends Model
{
    protected $table = 'switchfox_audit_transactions';

    protected $fillable = [
        'type',
        'endpoint',
        'internal_id',
        'amount',
        'request_data',
        'external_id',
        'response_code',
        'response_message',
        'response_datetime',
        'response_data',
    ];

    protected $casts = [
        'type' => 'string',
        'endpoint' => 'string',
        'internal_id' => 'string',
        'amount' => 'integer',
        'request_data' => 'json',
        'external_id' => 'string',
        'response_code' => 'string',
        'response_message' => 'string',
        'response_datetime' => 'datetime',
        'response_data' => 'json',
    ];

    public function getTable()
    {
        return config('switchfox.transaction_logging.prefix') . 'transactions';
    }
}
