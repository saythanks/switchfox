<?php

declare(strict_types=1);

namespace SayHi\Switchfox\Dto;

final class ResponseTransactionDto
{
    public function __construct(
        public string $external_id,
        public string $response_code,
        public string $response_message,
        public string $response_datetime,
        public array $response_data,
    ) {}
}
