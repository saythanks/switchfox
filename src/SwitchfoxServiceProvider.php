<?php

declare(strict_types=1);

namespace SayHi\Switchfox;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use SayHi\Switchfox\Console\Commands\FlushCacheCommand;

class SwitchfoxServiceProvider extends ServiceProvider
{
    public const DRIVER_DATABASE = 'database';

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/switchfox.php', 'switchfox');
    }

    public function boot(Filesystem $filesystem)
    {
        // This should be pulled out into a "driver" class structure with interfaces and constants
        if (config('switchfox.transaction_logging.enabled') && config('switchfox.transaction_logging.driver') === SwitchfoxServiceProvider::DRIVER_DATABASE) {
            $this->publishes([
                __DIR__ . '/../database/migrations/add_switchfox_audit_transaction_table.php.stub' => $this->getMigrationFileName($filesystem),
            ], 'migrations');
        }

        $this->publishes(
            [
                __DIR__ . '/config/switchfox.php' => config_path('switchfox.php'),
            ]);

        $this->commands([
            FlushCacheCommand::class,
        ]);
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path . '*_add_switchfox_audit_transaction_table.php');
            })->push($this->app->databasePath() . "/migrations/{$timestamp}_add_switchfox_audit_transaction_table.php")
            ->first();
    }
}
